
Gem::Specification.new do |s|
    s.name        = 'taball'
    s.version     = '0.0.0'
    s.date        = '2010-04-28'
    s.summary     = "Multiformat table generator."
    s.description = "A gem intended to tabulate any set of data introduced in a variety of formats."
    s.authors     = ["Juanpe Araque","A. Tejero"]
    s.email       = 'jp.araque@cern.ch'
    s.files       = ["lib/taball.rb","lib/format/format.rb"]
    s.license       = 'GPL'
end
