require 'format/format.rb'

module Taball
	class Table
		attr_accessor :rows, :columns, :cells

		def initialize(rows,columns)
			@rows = rows
			@columns = columns
			@cells = Hash.new {|h,k| h[k] = Hash.new}
		end

		def write
			(0..@rows-1).each do |row|
				(0..@columns-1).each do |col|
					print " #{cells[row][col].write} "
				end
				print "\n"
			end
		end

		def add_cell(x,y,text)
			@cells[x][y] = Cell.new do |cell|
				cell.text = text
				cell.x = x
				cell.y = y
			end
		end

		def new_row
			@rows += 1
		end

		def new_column
			@columns += 1
		end

		def index(x,y)
			@rows
		end
	end

	class Cell < Format
		attr_accessor :x, :y
		def initialize(args = {})
			# Default values before calling the block to initialize what needed
			super(args)
			@x = 0
			@y = 0
			yield self if block_given?
		end

		def write
			text
		end
	end

end
